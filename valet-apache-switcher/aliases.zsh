alias v2a="valet stop && sudo apachectl start && ps aux |egrep 'httpd|nginx'"
alias a2v="sudo apachectl stop && valet start && ps aux |egrep 'httpd|nginx'"
alias vrd="valet restart dnsmasq && dig x.test @127.0.0.1 && dig x.test"
