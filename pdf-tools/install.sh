# if ocrmypdf does not exist
if ! command -v ocrmypdf &> /dev/null; then
  brew install ocrmypdf
  # ocrmypdf install msa language
  brew install tesseract-lang
  brew install ghostscript
fi