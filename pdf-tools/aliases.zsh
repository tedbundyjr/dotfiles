# Function to OCR a PDF file with specified settings
pdf-ocr() {
  # Check if ocrmypdf is installed
  if ! command -v ocrmypdf &> /dev/null; then
    echo "Error: ocrmypdf is not installed. Please install it first."
    return 1
  fi

  # Check if a source PDF is provided
  if [[ -z "$1" ]]; then
    echo "Usage: pdf-ocr source.pdf"
    return 1
  fi

  # Define the source and output PDF file names
  local source_pdf="$1"
  local output_pdf="${source_pdf:r}-ocr.pdf"

  # Execute the OCR command
  ocrmypdf -l eng+msa --force-ocr --image-dpi 150 --jpeg-quality 75 --optimize 3 --clean --output-type pdf "$source_pdf" "$output_pdf"

  # Check if the OCR process was successful
  if [[ $? -eq 0 ]]; then
    echo "OCR completed successfully. Output saved as $output_pdf"
  else
    echo "Error: OCR process failed."
  fi
}

# Function to compress a PDF file using Ghostscript
pdf-compress() {

  # Check if a source PDF is provided
  if [[ -z "$1" ]]; then
    echo "Usage: pdf-compress input.pdf [output.pdf]"
    return 1
  fi

  # Define the source and output PDF file names
  local input_pdf="$1"
  local output_pdf="${2:-${input_pdf:r}-compressed.pdf}"

  # Execute the Ghostscript command to compress the PDF
  gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.7 -dPDFSETTINGS=/ebook -dNOPAUSE -dBATCH -dQUIET -sOutputFile="$output_pdf" "$input_pdf"

  # Check if the compression process was successful
  if [[ $? -eq 0 ]]; then
    echo "Compression completed successfully. Output saved as $output_pdf"
  else
    echo "Error: Compression process failed."
  fi
}